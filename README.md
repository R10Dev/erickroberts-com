## UI for erickroberts.com - Specifically designed for CMS purposes for the blog and portfolio portion of my site.

## The site is setup to use Sass, but it is built using tailwindcss via ng-tailwindcss
#### You can find the ["Quick and Dirty Setup" here](https://www.npmjs.com/package/ng-tailwindcss).

### There is also a really good vs code plugin to use with [tailwind code completion](https://marketplace.visualstudio.com/items?itemName=bradlc.vscode-tailwindcss).


