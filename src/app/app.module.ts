import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserFormComponent } from './components/user-form/user-form.component';
import { HttpClientModule} from '@angular/common/http';
import { ContentFormComponent } from './components/content-form/content-form.component';
import { UserLoginComponent } from './components/user-login/user-login.component';
import { ResumeComponent } from './components/resume/resume.component';
import { HeaderComponent } from './components/header/header.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { PortfolioHomeComponent } from './components/portfolio-home/portfolio-home.component';
import { BlogHomeComponent } from './components/blog-home/blog-home.component';
import { PodcastHomeComponent } from './components/podcast-home/podcast-home.component';
import { ContactComponent } from './components/contact/contact.component'; 
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    UserFormComponent,
    ContentFormComponent,
    UserLoginComponent,
    ResumeComponent,
    HeaderComponent,
    HomePageComponent,
    PortfolioHomeComponent,
    BlogHomeComponent,
    PodcastHomeComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule
  ],
  providers: [HeaderComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
