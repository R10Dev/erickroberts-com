import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserFormComponent } from './components/user-form/user-form.component';
import { UserLoginComponent } from './components/user-login/user-login.component';
import { PortfolioHomeComponent } from './components/portfolio-home/portfolio-home.component';
import { ResumeComponent } from './components/resume/resume.component';
import { BlogHomeComponent } from './components/blog-home/blog-home.component';
import { ContactComponent } from './components/contact/contact.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { PodcastHomeComponent } from './components/podcast-home/podcast-home.component';

const routes: Routes = [
  { path: '', component: HomePageComponent},
  { path: 'user-form', component: UserFormComponent},
  { path: 'user-login', component: UserLoginComponent},
  { path: 'portfolio-home', component: PortfolioHomeComponent},
  { path: 'resume', component: ResumeComponent},
  { path: 'blog-home', component: BlogHomeComponent},
  { path: 'contact', component: ContactComponent},
  { path: 'home', component: HomePageComponent},
  { path: 'podcast', component: PodcastHomeComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
